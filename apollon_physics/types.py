"""
Define types and utils for physics
"""
__license__ = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
import argparse
import doctest
# ~ import numpy

# ~ from physics.units import TemperatureUnit, DistanceUnit, VelocityUnit, TimeUnit, KELVIN_NM

##======================================================================================================================##
##                KEYS                                                                                                  ##
##======================================================================================================================##

_infoky = 'infos'
_nmkey = "name"
_sym_key = "symbol"


##======================================================================================================================##
##                                CLASSES                                                                               ##
##======================================================================================================================##

class PhysicConstant(float):
    """ Class doc
    >>> print(PhysicConstant(9, unit = "Pa"))
    9.0 Pa
    >>> print(PhysicConstant(9, unit = "Pa", name =  "p"))
    p = 9.0 Pa
    """
    def __new__(cls, value, **kwargs): #override the new method of float
        """
        Call when a new instance is defined
        """
        #~ print("in new")
        instance = super(PhysicConstant, cls).__new__(cls, value)
        instance.unit = None
        for key, val in kwargs.items():
            setattr(instance, key, val)
        #~ instance.unit = unit
        return instance

    # ~ def __init__(self, value, **kwargs):
        # ~ """Init object"""
        #~ print('in init')
        #~ print(self)
        #~ print(self.unit)

    def __str__(self):
        """Print representation of object"""
        if hasattr(self, _infoky):
            infos = getattr(self, _infoky)
            infos = f"{infos}\n"
        else:
            infos = ""

        if hasattr(self, _nmkey):
            name = getattr(self, _nmkey)
            name = f"{name} = "
            assert not hasattr(self, _sym_key)
        elif hasattr(self, _sym_key):
            name = getattr(self, _sym_key)
            name = f"{name} = "
        else:
            name = ""

        txt = f"{float(self)} {self.unit}"
        res = "".join([infos, name, txt])
        return res
        
    # ~ def get_unit(self):
        # ~ """Factice method"""
        # ~ res = self.unit
        # ~ return res
        

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    # ~ parser.add_argument('--examples', default=False, action='store_true')
    opts = parser.parse_args()


    #+++++++++++++++++++++++++++++++#
    #        Doctests               #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
        
        
    #+++++++++++++++++++++++++++++++#
    #    examples                   #
    #+++++++++++++++++++++++++++++++#

    print(PhysicConstant(9, unit = "Pa", name =  "p"))
