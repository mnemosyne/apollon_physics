"""
 SOme physical constant in SI units (International System of Units)
"""
__license__ = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import pandas
from measurement.measures import Temperature, Pressure

from apollon_physics.types import PhysicConstant

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

#TIMES          #
#---------------#

TD1S = pandas.Timedelta("1s")


#TEMPERATURE    #
#---------------#
ABSOLUTE_T0 = Temperature(k=0) #Temperature of absolute zero




#g              #
#---------------#

GRAV_ACCEL = PhysicConstant(name="g", value=9.81, unit="m s^(-2)", infos="gravity acceleration")


#RADIATIVE      #
#---------------#
STEFAN_BOLTZMANN_SIGMA = PhysicConstant(name="Sigma", value=5.670e-8, unit="W/m^2/K^4", infos='Stefan-Boltzmann constant')



#GAS             #
#---------------#
R_STAR = PhysicConstant(name="R*", value=8.31, unit="J/mol/K", infos="Perfect gas constant")


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##


if __name__ == '__main__':
    print(f"{R_STAR = }")
    print(f"{GRAV_ACCEL = :}")
    print(f"{STEFAN_BOLTZMANN_SIGMA = :}")
