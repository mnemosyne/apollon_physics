"""
_DESCRIPTION
"""
__license__ = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
import doctest

from apollon_physics.constants_si import STEFAN_BOLTZMANN_SIGMA


##======================================================================================================================##
##                _##
##======================================================================================================================##


def stefan_boltzmann_law(t_kelvin, epsilon=1):
    """
    >>> round(stefan_boltzmann_law(288),0)
    390.0
    >>> round(stefan_boltzmann_law(255),0)
    240.0
    >>> round(stefan_boltzmann_law(303),0)
    478.0
    >>> round(stefan_boltzmann_law(313),0)
    544.0
    >>> round(stefan_boltzmann_law(323),0)
    617.0
    """
    res = epsilon * STEFAN_BOLTZMANN_SIGMA * t_kelvin ** 4
    return res


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#

    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()

    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#

    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
