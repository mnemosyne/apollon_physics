"""
Physics module
Define type and variables
"""
__author__ = "Geremy Panthou"
__license__ = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

from apollon_physics.constants_si import *
from apollon_physics.radiation import *
from apollon_physics.types import PhysicConstant


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    import argparse
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--examples', default=False, action='store_true')
    opts = parser.parse_args()


    #+++++++++++++++++++++++++++++++#
    #        Doctests               #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        import doctest
        test_result = doctest.testmod()
        print(test_result)


    #+++++++++++++++++++++++++++++++#
    #    examples                   #
    #+++++++++++++++++++++++++++++++#
    if opts.examples:
        pass
